package com.codefiction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefiction.model.ConfirmationToken;
import com.codefiction.model.User;
import com.codefiction.repository.ConfirmationTokenRepository;
import com.codefiction.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	public List<User> findAll(){
		return repository.findAll();
	}
	
	public Optional<User> findById(Long id) {
		Optional<User> user = repository.findById(id);
		return user;
	}
	
	public User insert(User obj){
		User existingUser = repository.findByEmail(obj.getEmail());
		
		return repository.save(obj);
	}
	
	public Optional<User> deleteById(Long id) {
		Optional<User> user = this.findById(id);
		repository.deleteById(id);
		return user;
	}
	
	public User update(Long id, User obj) {
			User entity = repository.getOne(id);
			updateDate(entity, obj);
			return repository.save(entity);
	}
	
	private void updateDate(User entity, User obj) {
		entity.setUserName(obj.getUserName());
		entity.setEmail(obj.getEmail());
		entity.setPhone(obj.getPhone());

	}
	
	
}
