package com.codefiction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodefictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodefictionApplication.class, args);
	}

}
