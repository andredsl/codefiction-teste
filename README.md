<h1> Teste CODEFICTION<h1/>

Este projeto trabalha com a criação de um usuário, enviando um email de ativação para o email cadastrado, login do usuário retornando um jwt, e um CRUD do usuário utilizando o jwt.

<h2>Inicializando<h2/>

Para executar o projeto, será necessário instalar os seguintes programas:

[JDK 10: Necessário para executar o projeto Java](https://www.oracle.com/java/technologies/javase-downloads.html)
<br/>
[Maven 3.5.3: Necessário para realizar o build do projeto Java](https://maven.apache.org/install.html)

<h2>Desenvolvimento<h2/>

Para iniciar o desenvolvimento, é necessário clonar o projeto do GitLab num diretório de sua preferência:

`cd "diretorio de sua preferencia"`<br/>
`git clone https://gitlab.com/gitlab-tests/sample-project.git`

<h2>Contrução<h2/>

Para construir o projeto com o Maven, executar os comando abaixo:

`mvn clean install`

O comando irá baixar todas as dependências do projeto e criar um diretório target com os artefatos construídos, que incluem o arquivo jar do projeto. Além disso, serão executados os testes unitários, e se algum falhar, o Maven exibirá essa informação no console.

Também é necessário criar um bando de dados no postgreSQL. Na config do projeto o banco esta com o nome 'codefiction'.


<h2>Flow login<h2/>

Rota POST - /users/register "Espera um JSON com os dados do usuário, caso o usuário já exista ele não é cadastrado. Assim que realizar o cadastro é enviado um emial para o mesmo poder confirmar e ativar a conta."

Rota POST - /authenticate "realiza a autenticação do usuário, enviando um JSON com email e senha, caso a conta já tenha sido ativada através do e-mail, retorna um token valido para as requisições."

Rota GET - /users "Retorna uma lista de usuários cadastrados, é necessário um header 'Authorization' com o token válido para acessar a rota."

Rota GET - /users/id "Retorna um usuário específico referente ao id informado, é necessário um header 'Authorization' com o token válido para acessar a rota."

Rota PUT - /users/id "Altera os dados de um usuário específico referente ao id informado, é necessário um header 'Authorization' com o token válido para acessar a rota."

Rota DELETE - /users/id "Delete um usuário específico referente ao id informado, é necessário um header 'Authorization' com o token válido para acessar a rota."

Rota GET - /confirm "é acionado assim que o usuário clica no link enviado para o email cadastrado, informando que a conta foi ativada."

Rota GET - /welcome "uma pagina web, gerando um exemplo de login com escolha de idioma."

<h2>Faltou Fazer<h2/>

Logout, devido ao token ter um tempo para expirar.<br/>
Troca de senha pelo usuário.<br/>
Testes.
