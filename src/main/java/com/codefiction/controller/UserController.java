package com.codefiction.controller;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.codefiction.model.ConfirmationToken;
import com.codefiction.model.User;
import com.codefiction.repository.ConfirmationTokenRepository;
import com.codefiction.repository.UserRepository;
import com.codefiction.service.EmailSenderService;
import com.codefiction.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/users")
@Api(value = "API REST login")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private UserService service;
	
	@Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailSenderService emailSenderService;
    
    @Autowired
    private MessageSource messageSource;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma lista de usuários, necessário token de authenticação")
	public ResponseEntity<List<User>> findAll(@RequestHeader(value = "Authorization") String details){
		List<User> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	@GetMapping ("/{id}")
	@ApiOperation(value = "Retorna um usuário, necessário token de authenticação")
	public ResponseEntity<Optional<User>> findById(@PathVariable Long id, @RequestHeader(value = "Authorization") String details){
		Optional<User> user = service.findById(id);
		return ResponseEntity.ok().body(user);
	}
	
	@PostMapping("/register")
	@ApiOperation(value = "Realiza o cadastro de um usuário, necessário um json com as informações")
	public ModelAndView insert(@RequestBody User obj, ModelAndView modelAndView){
		User existingUser = userRepository.findByEmail(obj.getEmail());
		if(existingUser != null)
        {
            modelAndView.addObject("message","This email already exists!");
            modelAndView.setViewName("error");
        }
        else
        {
    	User user = service.insert(obj);
    	
    	ConfirmationToken confirmationToken = new ConfirmationToken(obj);
    	
    	confirmationTokenRepository.save(confirmationToken);
    	
    	SimpleMailMessage mailMessage = new SimpleMailMessage();
    	mailMessage.setTo(obj.getEmail());
        mailMessage.setSubject(messageSource.getMessage("complete.registration",null, Locale.forLanguageTag(obj.getLanguage())));
        mailMessage.setFrom("codefiction@gmail.com");
        mailMessage.setText(messageSource.getMessage("message.registration",null, Locale.forLanguageTag(obj.getLanguage()))
        +"http://localhost:8080/users/confirm?token="+confirmationToken.getConfirmationToken());
        
        emailSenderService.sendEmail(mailMessage);

        modelAndView.addObject("email", user.getEmail());
        
        modelAndView.addObject("welcome", messageSource.getMessage("welcome",null, Locale.forLanguageTag(obj.getLanguage())));
        modelAndView.addObject("message", messageSource.getMessage("hello",null, Locale.forLanguageTag(obj.getLanguage())));
        modelAndView.setViewName("successfulRegistration");
        }
		
		return modelAndView;
	}
	
	@RequestMapping(value="/confirm", method= {RequestMethod.GET, RequestMethod.POST})
	@ApiOperation(value = "Requisição realizada quando o usuário ativa sua conta através do email.")
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken)
    {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            User user = userRepository.findByEmail(token.getUser().getEmail());
            user.setEnabled(true);
            userRepository.save(user);
            modelAndView.addObject("welcome", messageSource.getMessage("welcome",null, Locale.forLanguageTag(user.getLanguage())));
            modelAndView.addObject("message", messageSource.getMessage("message.verification",null, Locale.forLanguageTag(user.getLanguage())));
            modelAndView.setViewName("verificationAccount");
        }
        else
        {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
        }

        return modelAndView;
    }
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Remove um usuário pelo seu id, necessário token de autenticação")
	public ResponseEntity<Optional<User>> delete(@PathVariable Long id, @RequestHeader(value = "Authorization") String details){
		Optional<User> user = service.deleteById(id);
		return ResponseEntity.ok().body(user);
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value = "Altera dados do usuário especificado pelo seu id")
	public ResponseEntity<User> update(@PathVariable Long id, @RequestBody User obj, @RequestHeader(value = "Authorization") String details){
		obj = service.update(id, obj);
		return ResponseEntity.ok().body(obj);
	}
	
	
	
	
}
