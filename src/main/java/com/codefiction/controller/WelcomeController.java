package com.codefiction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.codefiction.model.AuthRequest;
import com.codefiction.util.JwtUtil;

import io.swagger.annotations.ApiOperation;

@Controller
public class WelcomeController {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@GetMapping("/")
	public String welcome() {
		return "welcome";
	}
	
	@GetMapping("/welcome")
	@ApiOperation(value = "Retorna uma página simples de login, apenas para exemplo")
	public String welcomePage() {
		return "welcome";
	}
	
	@PostMapping(value = "/authenticate")
	@ApiOperation(value = "Realiza a autenticação do usuário, retornando um token válido")
	public ResponseEntity<String> generateToken(@RequestBody AuthRequest authRequest) throws Exception{
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getEmail(), authRequest.getPassword()));
		} catch (BadCredentialsException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("invalid username/password");
		}
		return ResponseEntity.ok().body(jwtUtil.generateToken(authRequest.getEmail()));
		
	}

}
